-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 18, 2022 at 02:32 PM
-- Server version: 5.7.36
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_jwd`
--

-- --------------------------------------------------------

--
-- Table structure for table `pengajar`
--

DROP TABLE IF EXISTS `pengajar`;
CREATE TABLE IF NOT EXISTS `pengajar` (
  `id` varchar(25) NOT NULL,
  `no_regis` varchar(50) NOT NULL,
  `name` varchar(25) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remove` varchar(10) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengajar`
--

INSERT INTO `pengajar` (`id`, `no_regis`, `name`, `phone`, `created_date`, `updated_date`, `remove`) VALUES
('9nmScTGbvPl81rzCxXVqF7w0', '082022-01', 'Sylvia', '08766617128', '2022-08-18 19:45:24', '2022-08-18 20:03:15', 'N'),
('r0jnegoUqikZ1H9GTIb2Ytfm', '082022-02', 'Rahmat Susanto', '08777629123971', '2022-08-18 19:46:01', '2022-08-18 20:03:24', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `peserta`
--

DROP TABLE IF EXISTS `peserta`;
CREATE TABLE IF NOT EXISTS `peserta` (
  `id` varchar(25) NOT NULL,
  `no_regis` varchar(50) NOT NULL,
  `name` varchar(25) NOT NULL,
  `gender` varchar(25) NOT NULL,
  `age` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `bday` date NOT NULL,
  `place` varchar(25) NOT NULL,
  `remove` varchar(10) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peserta`
--

INSERT INTO `peserta` (`id`, `no_regis`, `name`, `gender`, `age`, `address`, `created_date`, `updated_date`, `bday`, `place`, `remove`) VALUES
('ZPUcuJj9AFkl4rI5Hv6s1iL2', '150305899101-579', 'Ilham Akbari', 'lk', 24, 'Tanjung Priok, Jakarta Utara', '2022-08-12 17:03:03', '2022-08-18 18:41:49', '1998-05-30', 'Bandung', 'N'),
('9qSpnLQEW2vTNKYwkJrIfAUH', '150305899101-571', 'Fatimah', 'pr', 25, 'Kediri', '2022-08-12 17:16:43', '2022-08-18 18:41:17', '1997-06-18', 'Kediri', 'N'),
('jYDwU6K3NxP0cZoGJyrA21Fh', '150305899101-578', 'Ilham Akbari Winoto', 'lk', 24, 'Tanjung Priok, Jakarta Utara', '2022-08-18 18:54:09', '2022-08-18 18:54:09', '1998-05-30', 'Bandung', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `presensi`
--

DROP TABLE IF EXISTS `presensi`;
CREATE TABLE IF NOT EXISTS `presensi` (
  `id` varchar(25) NOT NULL,
  `tgl_hadir` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pertemuan_ke` int(11) NOT NULL,
  `kelas` varchar(50) NOT NULL,
  `id_peserta` varchar(25) NOT NULL,
  `id_pengajar` varchar(25) NOT NULL,
  `materi` varchar(50) NOT NULL,
  `bukti` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remove` varchar(5) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `presensi`
--

INSERT INTO `presensi` (`id`, `tgl_hadir`, `pertemuan_ke`, `kelas`, `id_peserta`, `id_pengajar`, `materi`, `bukti`, `created_date`, `updated_date`, `remove`) VALUES
('KRhfoYM5VWIv6cZLkaXUldq7', '2022-08-18 21:16:59', 11, 'JWD-B', 'ZPUcuJj9AFkl4rI5Hv6s1iL2', 'r0jnegoUqikZ1H9GTIb2Ytfm', 'Membuat fungsi CRUD', 'presensi/20220818211659_1.PNG', '2022-08-18 21:16:59', '2022-08-18 21:16:59', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `create_date`, `update_date`) VALUES
(1, 'akbar_jwd', 'akbariilham129@gmail.com', 'akbar_jwd', '2022-08-06 08:00:30', '2022-08-06 08:00:30');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
