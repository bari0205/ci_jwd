<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends CI_Controller {

	function __construct()
	{
		parent::__construct();
    	$this->load->model('T_Peserta');
        $this->load->library('session');
	}

	public function index()
	{
		if (empty($this->session->userdata('username'))) {
			$this->session->set_flashdata('title','login_first');
        	$this->session->set_flashdata('message','Please login first!');
        	$this->session->set_flashdata('status','error');
			return redirect('login/');
		}
		$data['page'] = 'crud_func';
		$data['cur_page'] = 'peserta';
		$data['datas'] = $this->T_Peserta->getAll();
		$this->load->view('peserta/index', $data);
	}

	public function add(){
		if (empty($this->session->userdata('username'))) {
			$this->session->set_flashdata('title','login_first');
        	$this->session->set_flashdata('message','Please login first!');
        	$this->session->set_flashdata('status','error');
			return redirect('login/');
		}
		$data['page'] = 'crud_func';
		$data['cur_page'] = 'peserta';
		$this->load->view('peserta/add', $data);			
	}

	public function create(){
		$id = random_string('alnum',24);
		$no_regis = $this->input->post('no_regis') ? $this->input->post('no_regis') : '';
		$name = $this->input->post('name') ? $this->input->post('name') : '';
		$gender = $this->input->post('gender') ? $this->input->post('gender') : '';
		$place = $this->input->post('place') ? $this->input->post('place') : '';
		$bday = $this->input->post('bday') ? $this->input->post('bday') : '';
		$bday1 = $bday ? new DateTime($bday) : '';
		$today = new DateTime("today");
		$age = $bday1 > $today ? "0" : $today->diff($bday1)->y;
		$add = $this->input->post('add') ? $this->input->post('add') : '';
		$now = date('Y-m-d H:i:s');

		$checkNum = $this->T_Peserta->checkNum($no_regis);
		if ($checkNum) {
        	$this->session->set_flashdata('title','action_failed');
        	$this->session->set_flashdata('message','Data No Register already used');
        	$this->session->set_flashdata('status','error');
        	redirect('/peserta/add');
        }else{
        	$datas = array(
				'id' => $id,
				'no_regis' => $no_regis,
				'name' => $name,
				'gender' => $gender,
				'place' => $place,
				'bday' => $bday,
				'age' => $age,
				'address' => $add,
				'created_date' => $now,
				'updated_date' => $now
			);

			$insert = $this->T_Peserta->insert($datas,'peserta');
			if ($insert) {
	        	$this->session->set_flashdata('title','action_success');
	        	$this->session->set_flashdata('message','Insert data');
	        	$this->session->set_flashdata('status','success');
	        	redirect('/peserta');
	        }else{
	        	$this->session->set_flashdata('title','action_failed');
	        	$this->session->set_flashdata('message','Insert data');
	        	$this->session->set_flashdata('status','error');
	        	redirect('/peserta/add');
	        }
        }
	}

	public function edit($id){
		if (empty($this->session->userdata('username'))) {
			$this->session->set_flashdata('title','login_first');
        	$this->session->set_flashdata('message','Please login first!');
        	$this->session->set_flashdata('status','error');
			return redirect('login/');
		}
		$data['page'] = 'crud_func';
		$data['cur_page'] = 'peserta';
		$data['datas'] = $this->T_Peserta->getDetail($id); 
		$this->load->view('peserta/edit', $data);			
	}

	public function update(){
		// $id = random_string('alnum',24);
		$id = $this->input->post('id') ? $this->input->post('id') : '';
		$no_regis_old = $this->input->post('no_regis_old') ? $this->input->post('no_regis_old') : '';
		$no_regis = $this->input->post('no_regis') ? $this->input->post('no_regis') : '';
		$name = $this->input->post('name') ? $this->input->post('name') : '';
		$gender = $this->input->post('gender') ? $this->input->post('gender') : '';
		$place = $this->input->post('place') ? $this->input->post('place') : '';
		$bday = $this->input->post('bday') ? $this->input->post('bday') : '';
		$bday1 = $bday ? new DateTime($bday) : '';
		$today = new DateTime("today");
		$age = $bday1 > $today ? "0 tahun" : $today->diff($bday1)->y;
		$add = $this->input->post('add') ? $this->input->post('add') : '';
		$now = date('Y-m-d H:i:s');
		if ($no_regis != $no_regis_old) {
			$checkNum = $this->T_Peserta->checkNum($no_regis);
			if ($checkNum) {
	        	$this->session->set_flashdata('title','action_failed');
	        	$this->session->set_flashdata('message','Data No Register already used');
	        	$this->session->set_flashdata('status','error');
	        	redirect('/peserta/edit/'.$id);
	        	die();
	        }
		}

		$datas = array(
			'name' => $name,
			'no_regis' => $no_regis,
			'gender' => $gender,
			'place' => $place,
			'bday' => $bday,
			'age' => $age,
			'address' => $add,
			'updated_date' => $now
		);

		$update = $this->T_Peserta->update($datas, $id);
		if ($update) {
        	$this->session->set_flashdata('title','action_success');
        	$this->session->set_flashdata('message','Update data');
        	$this->session->set_flashdata('status','success');
        	redirect('/peserta');
        }else{
        	$this->session->set_flashdata('title','action_failed');
        	$this->session->set_flashdata('message','Update data');
        	$this->session->set_flashdata('status','error');
        	redirect('/peserta/edit/'.$id);
        }
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$result = $this->T_Peserta->getDetail($id);
		if ($result) {
			$action = $this->T_Peserta->delete($id);
		}else{
			$action = false;
		}
		echo $action;
	}
}
