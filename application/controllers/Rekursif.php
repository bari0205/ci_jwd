<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekursif extends CI_Controller {
	
	public function index()
	{
		$data['page'] = 'pert8';
		$data['cur_page'] = 'rekursif';
		$this->load->view('rekursif', $data);

	}
}
