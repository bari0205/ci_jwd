<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	function __construct()
	{
		parent::__construct();
    	$this->load->model('T_User');
        $this->load->library('session');
	}

	public function index()
	{
		if (empty($this->session->userdata('username'))) {
			$this->session->set_flashdata('title','login_first');
        	$this->session->set_flashdata('message','Please login first!');
        	$this->session->set_flashdata('status','error');
			return redirect('login/');
		}
		$data['page'] = '';
		$data['cur_page'] = 'order';
		$this->load->view('order', $data);
	}
}
