<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biodata extends CI_Controller {

	function __construct()
	{
		parent::__construct();
    	$this->load->model('T_User');
        $this->load->library('session');
	}

	public function index()
	{
		if (empty($this->session->userdata('username'))) {
			$this->session->set_flashdata('title','login_first');
        	$this->session->set_flashdata('message','Please login first!');
        	$this->session->set_flashdata('status','error');
			return redirect('login/');
		}
		$data['page'] = '';
		$data['cur_page'] = 'biodata';
		$this->load->view('biodata', $data);
	}
	public function action(){
		$name = $this->input->post('name') ? $this->input->post('name') : '';
		$gender = $this->input->post('gender') ? $this->input->post('gender') : '';
		$place = $this->input->post('place') ? $this->input->post('place') : '';
		$bday = $this->input->post('bday') ? $this->input->post('bday') : '';
		$bday1 = $bday ? new DateTime($bday) : '';
		$today = new DateTime("today");
		$age = $bday1 > $today ? "0 tahun" : $today->diff($bday1)->y;
		$add = $this->input->post('add') ? $this->input->post('add') : '';

		if ($name=='' && $gender =='' && $place == '' && $bday=='') {
		header("location: ".base_url()."biodata?data=kosong");
		}
		else if ($name!='' || $gender !='' || $place != '' || $bday!=''){
			header("location:".base_url()."biodata?name=".$name."&gender=".$gender."&place=".$place."&bday=".$bday."&add=".$add."&age=".$age."&data=true");
		}
	}
}
