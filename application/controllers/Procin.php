<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Procin extends CI_Controller {
	
	public function index()
	{
		$data['page'] = 'pert8';
		$data['cur_page'] = 'procin';
		$this->load->view('procin', $data);

	}
}
