<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presensi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
    	$this->load->model('T_Peserta');
    	$this->load->model('T_Pengajar');
    	$this->load->model('T_Presensi');
        $this->load->library('session');
	}

	public function index()
	{
		if (empty($this->session->userdata('username'))) {
			$this->session->set_flashdata('title','login_first');
        	$this->session->set_flashdata('message','Please login first!');
        	$this->session->set_flashdata('status','error');
			return redirect('login/');
		}
		$data['page'] = 'crud_func';
		$data['cur_page'] = 'presensi';
		$data['datas'] = $this->T_Presensi->getAll();
		$this->load->view('presensi/index', $data);
	}

	public function add(){
		if (empty($this->session->userdata('username'))) {
			$this->session->set_flashdata('title','login_first');
        	$this->session->set_flashdata('message','Please login first!');
        	$this->session->set_flashdata('status','error');
			return redirect('login/');
		}
		$data['page'] = 'crud_func';
		$data['cur_page'] = 'presensi';
		$data['data_peserta'] = $this->T_Peserta->getAll();
		$data['data_pengajar'] = $this->T_Pengajar->getAll();
		$this->load->view('presensi/add', $data);			
	}

	public function create(){
		$id = random_string('alnum',24);
		$id_peserta = $this->input->post('id_peserta') ? $this->input->post('id_peserta') : '';
		$id_pengajar = $this->input->post('id_pengajar') ? $this->input->post('id_pengajar') : '';
		$meet = $this->input->post('meet') ? $this->input->post('meet') : '';
		$class = $this->input->post('class') ? $this->input->post('class') : '';
		$materi = $this->input->post('materi') ? $this->input->post('materi') : '';
		$now = date('Y-m-d H:i:s');
		$now_file = date('YmdHis');
		// var_dump($id_peserta);
		// die();

		$edit_name = str_replace(' ', '-', $_FILES["bukti"]['name']);
		$bukti = $now_file.'_'.$edit_name;
		
		$config['upload_path']          = FCPATH.'assets/uploads/presensi';
        // $config['allowed_types']        = 'gif|jpg|png';
        $config['allowed_types']        = '*';
        $config['max_size']             = 10000;
        $config['file_name']            = $bukti;
		$config['overwrite']            = true;
		$this->load->library('upload', $config);


		$check = $this->T_Presensi->check($id_peserta);
		if ($check) {
        	$this->session->set_flashdata('title','action_failed');
        	$this->session->set_flashdata('message','User already checkin');
        	$this->session->set_flashdata('status','error');
        	redirect('/presensi/add');
        }else{
        	if ($_FILES["bukti"]['name'] == '' || $_FILES["bukti"]['name'] == NULL ) {
	        	$this->session->set_flashdata('title','action_failed');
	        	$this->session->set_flashdata('message','Bukti Hadir Not Found');
	        	$this->session->set_flashdata('status','error');
	        	redirect('/presensi/add');
	        	die();
	    	}else{
	    		if ( ! $this->upload->do_upload('bukti'))
		        {
		            $error = array('error' => $this->upload->display_errors());
		            $this->session->set_flashdata('title','action_failed');
		        	$this->session->set_flashdata('message','File type not support. Please Try again!');
		        	$this->session->set_flashdata('status','error');
		        	redirect($_SERVER['HTTP_REFERER']);
		        }
	    		$datas = array(
					'id' => $id,
					'id_peserta' => $id_peserta,
					'id_pengajar' => $id_pengajar,
					'pertemuan_ke' => $meet,
					'kelas' => $class,
					'materi' => $materi,
					'bukti' => 'presensi/'.$bukti,
					'created_date' => $now,
					'updated_date' => $now
				);

				$insert = $this->T_Presensi->insert($datas,'presensi');
				if ($insert) {
		        	$this->session->set_flashdata('title','action_success');
		        	$this->session->set_flashdata('message','Insert data');
		        	$this->session->set_flashdata('status','success');
		        	redirect('/presensi');
		        }else{
		        	$this->session->set_flashdata('title','action_failed');
		        	$this->session->set_flashdata('message','Insert data');
		        	$this->session->set_flashdata('status','error');
		        	redirect('/presensi/add');
		        }
	    	}
        }
	}

	public function edit($id){
		if (empty($this->session->userdata('username'))) {
			$this->session->set_flashdata('title','login_first');
        	$this->session->set_flashdata('message','Please login first!');
        	$this->session->set_flashdata('status','error');
			return redirect('login/');
		}
		$data['page'] = 'crud_func';
		$data['cur_page'] = 'presensi';
		$data['datas'] = $this->T_Presensi->getDetail($id); 
		$data['data_peserta'] = $this->T_Peserta->getAll();
		$data['data_pengajar'] = $this->T_Pengajar->getAll();
		$this->load->view('presensi/edit', $data);			
	}

	public function update(){
		// $id = random_string('alnum',24);
		$id = $this->input->post('id') ? $this->input->post('id') : '';
		$id_peserta = $this->input->post('id_peserta') ? $this->input->post('id_peserta') : '';
		$id_pengajar = $this->input->post('id_pengajar') ? $this->input->post('id_pengajar') : '';
		$meet = $this->input->post('meet') ? $this->input->post('meet') : '';
		$class = $this->input->post('class') ? $this->input->post('class') : '';
		$materi = $this->input->post('materi') ? $this->input->post('materi') : '';
		$now = date('Y-m-d H:i:s');
		$now_file = date('YmdHis');

		$edit_name = str_replace(' ', '-', $_FILES["bukti"]['name']);
		$bukti = $now_file.'_'.$edit_name;
		
		$config['upload_path']          = FCPATH.'assets/uploads/presensi';
        // $config['allowed_types']        = 'gif|jpg|png';
        $config['allowed_types']        = '*';
        $config['max_size']             = 10000;
        $config['file_name']            = $bukti;
		$config['overwrite']            = true;
		$this->load->library('upload', $config);

		if ($_FILES["bukti"]['name'] == '' || $_FILES["bukti"]['name'] == NULL ) {
			$datas = array(
				'id' => $id,
				'id_pengajar' => $id_pengajar,
				'pertemuan_ke' => $meet,
				'kelas' => $class,
				'materi' => $materi,
				'updated_date' => $now
			);

			$update = $this->T_Presensi->update($datas, $id);
			if ($update) {
	        	$this->session->set_flashdata('title','action_success');
	        	$this->session->set_flashdata('message','Update data');
	        	$this->session->set_flashdata('status','success');
	        	redirect('/presensi');
	        }else{
	        	$this->session->set_flashdata('title','action_failed');
	        	$this->session->set_flashdata('message','Update data');
	        	$this->session->set_flashdata('status','error');
	        	redirect('/presensi/edit/'.$id);
	        }
    	}else{
    		if ( ! $this->upload->do_upload('bukti'))
	        {
	            $error = array('error' => $this->upload->display_errors());
	            $this->session->set_flashdata('title','action_failed');
	        	$this->session->set_flashdata('message','File type not support. Please Try again!');
	        	$this->session->set_flashdata('status','error');
	        	redirect($_SERVER['HTTP_REFERER']);
	        }
	 //        var_dump($bukti);
		// die();
    		$datas = array(
				'id' => $id,
				'id_pengajar' => $id_pengajar,
				'pertemuan_ke' => $meet,
				'kelas' => $class,
				'materi' => $materi,
				'bukti' => 'presensi/'.$bukti,
				'updated_date' => $now
			);

			$update = $this->T_Presensi->update($datas, $id);
			if ($update) {
	        	$this->session->set_flashdata('title','action_success');
	        	$this->session->set_flashdata('message','Update data');
	        	$this->session->set_flashdata('status','success');
	        	redirect('/presensi');
	        }else{
	        	$this->session->set_flashdata('title','action_failed');
	        	$this->session->set_flashdata('message','Update data');
	        	$this->session->set_flashdata('status','error');
	        	redirect('/presensi/edit/'.$id);
	        }
    	}
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$result = $this->T_Presensi->getDetail($id);
		if ($result) {
			$action = $this->T_Presensi->delete($id);
		}else{
			$action = false;
		}
		echo $action;
	}
}
