<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prosedur extends CI_Controller {
	
	public function index()
	{
		$data['page'] = 'pert8';
		$data['cur_page'] = 'proc';
		$this->load->view('prosedur', $data);

	}
}
