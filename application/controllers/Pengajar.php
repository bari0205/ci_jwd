<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajar extends CI_Controller {

	function __construct()
	{
		parent::__construct();
    	$this->load->model('T_Pengajar');
        $this->load->library('session');
	}

	public function index()
	{
		if (empty($this->session->userdata('username'))) {
			$this->session->set_flashdata('title','login_first');
        	$this->session->set_flashdata('message','Please login first!');
        	$this->session->set_flashdata('status','error');
			return redirect('login/');
		}
		$data['page'] = 'crud_func';
		$data['cur_page'] = 'pengajar';
		$data['datas'] = $this->T_Pengajar->getAll();
		$this->load->view('pengajar/index', $data);
	}

	public function add(){
		if (empty($this->session->userdata('username'))) {
			$this->session->set_flashdata('title','login_first');
        	$this->session->set_flashdata('message','Please login first!');
        	$this->session->set_flashdata('status','error');
			return redirect('login/');
		}
		$data['page'] = 'crud_func';
		$data['cur_page'] = 'pengajar';
		$this->load->view('pengajar/add', $data);			
	}

	public function create(){
		$id = random_string('alnum',24);
		$no_regis = $this->input->post('no_regis') ? $this->input->post('no_regis') : '';
		$name = $this->input->post('name') ? $this->input->post('name') : '';
		$phone = $this->input->post('phone') ? $this->input->post('phone') : '';
		$now = date('Y-m-d H:i:s');

		$checkNum = $this->T_Pengajar->checkNum($no_regis);
		if ($checkNum) {
        	$this->session->set_flashdata('title','action_failed');
        	$this->session->set_flashdata('message','Data No Register already used');
        	$this->session->set_flashdata('status','error');
        	redirect('/pengajar/add');
        }else{
        	$datas = array(
				'id' => $id,
				'no_regis' => $no_regis,
				'name' => $name,
				'phone' => $phone,
				'created_date' => $now,
				'updated_date' => $now
			);

			$insert = $this->T_Pengajar->insert($datas,'pengajar');
			if ($insert) {
	        	$this->session->set_flashdata('title','action_success');
	        	$this->session->set_flashdata('message','Insert data');
	        	$this->session->set_flashdata('status','success');
	        	redirect('/pengajar');
	        }else{
	        	$this->session->set_flashdata('title','action_failed');
	        	$this->session->set_flashdata('message','Insert data');
	        	$this->session->set_flashdata('status','error');
	        	redirect('/pengajar/add');
	        }
        }
	}

	public function edit($id){
		if (empty($this->session->userdata('username'))) {
			$this->session->set_flashdata('title','login_first');
        	$this->session->set_flashdata('message','Please login first!');
        	$this->session->set_flashdata('status','error');
			return redirect('login/');
		}
		$data['page'] = 'crud_func';
		$data['cur_page'] = 'pengajar';
		$data['datas'] = $this->T_Pengajar->getDetail($id); 
		$this->load->view('pengajar/edit', $data);			
	}

	public function update(){
		// $id = random_string('alnum',24);
		$id = $this->input->post('id') ? $this->input->post('id') : '';
		$no_regis_old = $this->input->post('no_regis_old') ? $this->input->post('no_regis_old') : '';
		$no_regis = $this->input->post('no_regis') ? $this->input->post('no_regis') : '';
		$name = $this->input->post('name') ? $this->input->post('name') : '';
		$phone = $this->input->post('phone') ? $this->input->post('phone') : '';
		$now = date('Y-m-d H:i:s');
		if ($no_regis != $no_regis_old) {
			$checkNum = $this->T_Pengajar->checkNum($no_regis);
			if ($checkNum) {
	        	$this->session->set_flashdata('title','action_failed');
	        	$this->session->set_flashdata('message','Data No Register already used');
	        	$this->session->set_flashdata('status','error');
	        	redirect('/pengajar/edit/'.$id);
	        	die();
	        }
		}

		$datas = array(
			'name' => $name,
			'no_regis' => $no_regis,
			'phone' => $phone,
			'updated_date' => $now
		);

		$update = $this->T_Pengajar->update($datas, $id);
		if ($update) {
        	$this->session->set_flashdata('title','action_success');
        	$this->session->set_flashdata('message','Update data');
        	$this->session->set_flashdata('status','success');
        	redirect('/pengajar');
        }else{
        	$this->session->set_flashdata('title','action_failed');
        	$this->session->set_flashdata('message','Update data');
        	$this->session->set_flashdata('status','error');
        	redirect('/pengajar/edit/'.$id);
        }
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$result = $this->T_Pengajar->getDetail($id);
		if ($result) {
			$action = $this->T_Pengajar->delete($id);
		}else{
			$action = false;
		}
		echo $action;
	}
}
