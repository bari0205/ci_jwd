<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
    	$this->load->model('T_User');
        $this->load->library('session');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function action(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$check = $this->T_User->get_detail($username, $password);
		if (empty($check)) {
			$this->session->set_flashdata('title','failed');
        	$this->session->set_flashdata('message','Invalid Username or Password. Please Try again!');
        	$this->session->set_flashdata('status','error');
        	return redirect('/login');
		}
		else{
			$this->session->set_userdata('id',$check[0]->id);
            $this->session->set_userdata('username',$check[0]->username);
		}
		redirect('/peserta');
	}

	public function logout(){
		$this->session->sess_destroy();
        redirect('/login/');
	}
}
