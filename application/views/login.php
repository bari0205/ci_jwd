<!DOCTYPE html>
<html lang="en">
<head>
	<title>Pertemuan 7 | Login Page</title>
	<?php $this->load->view('packages/head'); ?>
</head>
<body>
	<div class="container mt-3">
		<div class="row justify-content-center">
			<img style="width: 10%;" src="<?=base_url();?>assets/images/letter-i.png">
		</div>
		<div class="row justify-content-center">
			<div class="card shadow-sm my-5" style="width: 30rem;">
			  <div class="card-body">
			    <h5 class="card-title">Login</h5>
			    <?php 
			    	if ($this->session->flashdata('title') == 'failed') {
			     ?>
			    <div class="alert alert-danger" role="alert">
				  <?=$this->session->flashdata('message');?>
				</div>
				<?php }?>
			    <div>
			    	<form method="POST" action="<?=base_url();?>login/action" onsubmit="return validation();">
			    		<div class="form-group">
						   <label for="exampleInputEmail1">Username</label>
						   <input type="text" name="username" class="form-control" id="username" placeholder="Username Anda">
						</div>

						<div class="form-group">
						   <label for="">Password</label>
						   <input type="password" name="password" class="form-control" id="password" placeholder="Password Anda">
						</div>

						<div class="form-group" style="text-align: right; padding: 5px;">
							<button type="submit" class="btn btn-primary">login</button>
						</div>
			    	</form>
			    </div>
			    
			  </div>
			</div>
		</div>
	</div>
	<!-- Footer -->
	<?php $this->load->view('packages/footer'); ?>
	<script type="text/javascript">
		function validation(){
			var username = $('#username');
			var password = $('#password');
			// console.log(bday);
			// return false;
			if (username.val() == '') {
				alert('Username wajib diisi');
				username.focus();
				return false;
			}

	        if (password.val() == '') {
	        	alert('Password wajib diisi');
	        	password.focus();
				return false;
	        }
		}	
	</script>
	
</body>
</html>
