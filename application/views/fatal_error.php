<!DOCTYPE html>
<html lang="en">
<head>
	<title>Pertemuan 8 | Functional Procedure</title>
	<?php $this->load->view('packages/head'); ?>
</head>
<body>
	<?php $this->load->view('parts/header'); ?>
	<div class="container mt-3">
		<div class="row justify-content-start">

			<div class="col-md-12">
				<br>
				<hr>
				<br>
				<h5>Nilai Default</h5>
			</div>
			<div class="col-md-12">
				<?php 
					function perkenalandiri($nama, $salam='Selamat datang'){
					echo $salam.', ';
					echo "Nama saya ".$nama."<br>";
					echo "Senang berkenalan dengan anda<br>";
				}
				function perkenalandiri($nama, $salam='Selamat datang'){
					echo $salam.', ';
					echo "Nama saya ".$nama."<br>";
					echo "Senang berkenalan dengan anda<br>";
				}
 
					perkenalandiri("Akbar");
				?>
			</div>
			
			<br><br>

		</div>
	</div>
	<!-- Footer -->
	<?php $this->load->view('packages/footer'); ?>
</body>
</html>
