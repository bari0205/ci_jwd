<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.min.js" integrity="sha256-lSjKY0/srUM9BE3dPm+c4fBo1dky2v27Gdjm2uoZaL0=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://rsudcimacan.cianjurkab.go.id/assets/fe/vendor/glightbox/js/glightbox.min.js"></script>
<!-- <script defer src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" integrity="sha384-rOA1PnstxnOBLzCLMcre8ybwbTmemjzdNlILg8O7z1lUkLXozs4DHonlDtnE7fpc" crossorigin="anonymous"></script> -->
<script type="text/javascript">
	$(document).ready(function () {
	    $('#data-table').DataTable();
	    const galleryLightbox = GLightbox({
        selector: '.gallery-lightbox'
      });
	});
</script>
<?php if ($this->session->flashdata('title') == 'login_first') {?>
	<script type="text/javascript">
		swal("Error", "<?=$this->session->flashdata('message');?>", "error");
	</script>
<?php } ?>

<?php if ($this->session->flashdata('title') == 'action_success') {?>
	<script type="text/javascript">
		swal("Success", "<?=$this->session->flashdata('message');?>", "success");
	</script>
<?php }else if($this->session->flashdata('title') == 'action_failed'){ ?>
	<script type="text/javascript">
		swal("Failed", "<?=$this->session->flashdata('message');?>", "error");
	</script>
<?php } ?>