<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tugas Data Entry</title>
	<?php $this->load->view('packages/head'); ?>
</head>
<body>
	<?php $this->load->view('parts/header'); ?>
	<div class="container mt-3">
		<div class="row">
			<div class="col-md-5">
				<h2>Tambah Biodata Peserta</h2>
				<!-- <br> -->
				<form method="POST" action="<?=base_url();?>peserta/create" onsubmit="return validation()">
					<div class="form-group">
					   <label for="exampleInputEmail1">No Registrasi</label>
					   <input type="text" name="no_regis" class="form-control" id="no_regis" placeholder="No Registrasi Anda">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Nama</label>
					   <input type="text" name="name" class="form-control" id="name" placeholder="Nama Anda">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Jenis Kelamin</label>
					   <br>
					   <input type="radio" name="gender" id="gender" value="lk"> Laki-laki
					   <input type="radio" name="gender" id="gender" value="pr"> Perempuan
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Tempat Lahir</label>
					   <input type="text" name="place" class="form-control" id="place" placeholder="Tempat lahir anda">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Tempat Lahir</label>
					   <input type="date" name="bday" class="form-control" id="bday" placeholder="Tempat lahir anda">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Alamat</label>
					   <textarea class="form-control" name="add" id="add" placeholder="Alamat Anda"></textarea>
					</div>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</form>				
			</div>
		</div>
	</div>
	<!-- Footer -->
	<?php $this->load->view('packages/footer'); ?>
	<script type="text/javascript">
		function validation(){
			var no_regis = $('#no_regis');
			var name = $('#name');
			var gender = $('input[name="gender"]');
			var genderCheck = false;
			var place = $('#place');
			var bday = $('#bday').val();
			var add = $('#add');
			console.log(gender.length);
			// return false;
			if (no_regis.val() == '') {
				alert('No Registrasi wajib diisi');
				no_regis.focus();
				return false;
			}

			if (name.val() == '') {
				alert('Nama wajib diisi');
				name.focus();
				return false;
			}

			for(var i=0; i<gender.length;i++){
	            if(gender[i].checked == true){
					genderCheck = true;	            	     
	            }
	        }

	        if (genderCheck == false) {
	        	alert('Jenis kelamin wajib diisi');
	            return false;
	        }

	        if (place.val() == '') {
	        	alert('Tempat lahir wajib diisi');
				place.focus();
				return false;
	        }

	        if (bday == '') {
	        	alert('Tanggal lahir wajib diisi');
				return false;
	        }

	        if (add.val() == '') {
	        	alert('Alamat wajib diisi');
	        	add.focus();
				return false;
	        }
		}	
	</script>
</body>
</html>
