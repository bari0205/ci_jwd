<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tugas Data Entry</title>
	<?php $this->load->view('packages/head'); ?>
</head>
<body>
	<?php $this->load->view('parts/header'); ?>
	<div class="container mt-3">
		<div class="row">
			<div class="col-md-12">
				<!-- <br> -->
				<div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold primary-color">Biodata Peserta</h6>
                  <a href="<?=base_url('peserta/add')?>" class="btn btn-primary"><span><i class="fa fa-plus"></i></span> Add Data</a>
                </div>
                <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="data-table" style="font-size: 14px;">
                    <thead class="thead-light">
                      <tr>
                        <th>#</th>
                        <th>No. Registrasi</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Age</th>
                        <th>Birthday</th>
                        <th>Address</th>
                        <th>Update Date</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="">
                      <?php $num = 0; foreach ($datas as $data) { $num++;?>
                      <tr class="sort-wrap" data-snum="<?=$num;?>" data-sid="<?=$data->id;?>">
                        <td style="width: 10%;"><?=$num;?></td>
                        <td><?=$data->no_regis;?></td>
                        <td><?=$data->name;?></td>
                        <td><?=$data->gender=='lk' ? 'Laki-laki' : 'Perempuan';?></td>
                        <td><?=$data->age;?> Tahun</td>
                        <td><?=$data->place;?>, <?=date('d F Y', strtotime($data->bday));?></td>
                        <td><?=$data->address;?></td>
                        <td><?=date('d F Y', strtotime($data->updated_date));?></td>
                        <td>
                          <button onclick="confirmDelete('<?=$data->id;?>')" style="font-size: 12px;" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                          <a href="<?=base_url('peserta/edit/').$data->id;?>" style="font-size: 12px;" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                        </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>							
			</div>
		</div>
	</div>
	<!-- Footer -->
	<?php $this->load->view('packages/footer'); ?>
	<script type="text/javascript">
		function validation(){
			var name = $('#name');
			var gender = $('#gender');
			var genderCheck = false;
			var place = $('#place');
			var bday = $('#bday').val();
			var add = $('#add');
			// console.log(bday);
			// return false;
			if (name.val() == '') {
				alert('Nama wajib diisi');
				name.focus();
				return false;
			}

			for(var i=0; i<gender.length;i++){
	            if(gender[i].checked == true){
					genderCheck = true;	            	     
	            }
	        }

	        if (genderCheck == false) {
	        	alert('Jenis kelamin wajib diisi');
	            return false;
	        }

	        if (place.val() == '') {
	        	alert('Tempat lahir wajib diisi');
				place.focus();
				return false;
	        }

	        if (bday == '') {
	        	alert('Tanggal lahir wajib diisi');
				return false;
	        }

	        if (add.val() == '') {
	        	alert('Alamat wajib diisi');
	        	add.focus();
				return false;
	        }
		}	


		function confirmDelete(id){
	      swal({
	        title: "Are you sure?",
	        text: "Once deleted, you will not be able to recover this data!",
	        icon: "warning",
	        buttons: true,
	        dangerMode: true,
	        closeOnCancel: true
	      })
	      .then((willDelete) => {
	        if (willDelete) {
	          var url = "<?=base_url('peserta/delete')?>";
	          $.ajax({
	            type:'POST',
	            url: url, 
	            data : {id: id}, 
	            success:function(result){
	              // console.log(result);
	              if (result == 1) {
	                swal({
	                  title: "Success",
	                  text: "Deleted data",
	                  icon: "success",
	                  button: "Ok",
	                }).then((isconfirm) => {
	                  if (isconfirm) {
	                    location.reload();
	                  }else{
	                    
	                  }
	                });
	              }else{
	                swal({
	                  title: "Failed",
	                  text: "Deleted data. Please try again",
	                  icon: "error",
	                  button: "Ok",
	                });
	              }
	            }
	          });

	        } else {
	        }
	      });
	    }
	</script>
	
</body>
</html>
