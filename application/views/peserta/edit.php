<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tugas Data Entry</title>
	<?php $this->load->view('packages/head'); ?>
</head>
<body>
	<?php $this->load->view('parts/header'); ?>
	<div class="container mt-3">
		<div class="row">
			<div class="col-md-5">
				<h2>Tambah Biodata Peserta</h2>
				<!-- <br> -->
				<form method="POST" action="<?=base_url();?>peserta/update" onsubmit="return validation()">
					<input type="hidden" name="id" value="<?=$datas[0]->id;?>">
					<input type="hidden" name="no_regis_old" value="<?=$datas[0]->no_regis;?>">
					<div class="form-group">
					   <label for="exampleInputEmail1">No Registrasi</label>
					   <input type="text" name="no_regis" class="form-control" id="no_regis" placeholder="No Registrasi Anda" value="<?=$datas[0]->no_regis;?>">
					</div>
					<div class="form-group">
					   <label for="exampleInputEmail1">Nama</label>
					   <input type="text" name="name" class="form-control" id="name" placeholder="Nama Anda" value="<?=$datas[0]->name;?>">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Jenis Kelamin</label>
					   <br>
					   <input type="radio" name="gender" id="gender" value="lk" <?=$datas[0]->gender=='lk' ? 'checked' : '';?>> Laki-laki
					   <input type="radio" name="gender" id="gender" value="pr" <?=$datas[0]->gender=='pr' ? 'checked' : '';?>> Perempuan
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Tempat Lahir</label>
					   <input type="text" name="place" class="form-control" id="place" placeholder="Tempat lahir anda" value="<?=$datas[0]->place;?>">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Tempat Lahir</label>
					   <input type="date" name="bday" class="form-control" id="bday" placeholder="Tempat lahir anda" value="<?=$datas[0]->bday;?>">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Alamat</label>
					   <textarea class="form-control" name="add" id="add" placeholder="Alamat Anda"><?=$datas[0]->address;?></textarea>
					</div>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</form>				
			</div>
		</div>
	</div>
	<!-- Footer -->
	<?php $this->load->view('packages/footer'); ?>
	<script type="text/javascript">
		function validation(){
			var name = $('#name');
			var gender = $('input[name="gender"]');
			var genderCheck = false;
			var place = $('#place');
			var bday = $('#bday').val();
			var add = $('#add');
			// console.log(bday);
			// return false;
			if (name.val() == '') {
				alert('Nama wajib diisi');
				name.focus();
				return false;
			}

			for(var i=0; i<gender.length;i++){
	            if(gender[i].checked == true){
					genderCheck = true;	            	     
	            }
	        }

	        if (genderCheck == false) {
	        	alert('Jenis kelamin wajib diisi');
	            return false;
	        }

	        if (place.val() == '') {
	        	alert('Tempat lahir wajib diisi');
				place.focus();
				return false;
	        }

	        if (bday == '') {
	        	alert('Tanggal lahir wajib diisi');
				return false;
	        }

	        if (add.val() == '') {
	        	alert('Alamat wajib diisi');
	        	add.focus();
				return false;
	        }
		}	
	</script>
	
</body>
</html>
