<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tugas Data Entry</title>
	<?php $this->load->view('packages/head'); ?>
</head>
<body>
	<?php $this->load->view('parts/header'); ?>
	<div class="container mt-3">
		<div class="row">
			<div class="col-md-5">
				<h2>Tambah Biodata Pengajar</h2>
				<!-- <br> -->
				<form method="POST" action="<?=base_url();?>pengajar/update" onsubmit="return validation()">
					<input type="hidden" name="id" value="<?=$datas[0]->id;?>">
					<input type="hidden" name="no_regis_old" value="<?=$datas[0]->no_regis;?>">
					<div class="form-group">
					   <label for="exampleInputEmail1">No Registrasi</label>
					   <input type="text" name="no_regis" class="form-control" id="no_regis" placeholder="No Registrasi Anda" value="<?=$datas[0]->no_regis;?>">
					</div>
					<div class="form-group">
					   <label for="exampleInputEmail1">Nama</label>
					   <input type="text" name="name" class="form-control" id="name" placeholder="Nama Anda" value="<?=$datas[0]->name;?>">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Nomor Ponsel</label>
					   <input type="text" name="phone" class="form-control" id="phone" placeholder="Nomor Ponsel anda" value="<?=$datas[0]->phone;?>">
					</div>

					<button type="submit" class="btn btn-primary">Simpan</button>
				</form>				
			</div>
		</div>
	</div>
	<!-- Footer -->
	<?php $this->load->view('packages/footer'); ?>
	<script type="text/javascript">
		function validation(){
			var no_regis = $('#no_regis');
			var name = $('#name');
			var phone = $('#phone');
			console.log(gender.length);
			// return false;
			if (no_regis.val() == '') {
				alert('No Registrasi wajib diisi');
				no_regis.focus();
				return false;
			}

			if (name.val() == '') {
				alert('Nama wajib diisi');
				name.focus();
				return false;
			}

	        if (phone.val() == '') {
	        	alert('Nomor Ponsel wajib diisi');
				phone.focus();
				return false;
	        }
		}	
	</script>
	
</body>
</html>
