<!DOCTYPE html>
<html lang="en">
<head>
	<title>Pertemuan 6 | Tugas 1</title>
	<?php $this->load->view('packages/head'); ?>
</head>
<body>
	<?php $this->load->view('parts/header'); ?>
	<?php 
		$data = $this->input->get('data') ? $this->input->get('data') : '';
		$name = $this->input->get('name') ? $this->input->get('name') : '';
		$gender = $this->input->get('gender') ? $this->input->get('gender') : '';
		$place = $this->input->get('place') ? $this->input->get('place') : '';
		$bday = $this->input->get('bday') ? $this->input->get('bday') : '';
		$age = $this->input->get('age') ? $this->input->get('age') : '';
		$add = $this->input->get('add') ? $this->input->get('add') : '';
	?>
	<div class="container mt-3">
		<div class="row">
			<div class="col-md-5">
				<h2>Biodata Sederhana</h2>
				<?php if ($data=='kosong') {?>
				<div class="alert alert-danger" role="alert">
				  Data kosong!
				</div>
				<?php } ?>
				<!-- <br> -->
				<form method="POST" action="<?=base_url();?>biodata/action" onsubmit="return validation()">
					<div class="form-group">
					   <label for="exampleInputEmail1">Nama</label>
					   <input type="text" name="name" class="form-control" id="name" placeholder="Nama Anda">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Jenis Kelamin</label>
					   <br>
					   <input type="radio" name="gender" id="gender" value="lk"> Laki-laki
					   <input type="radio" name="gender" id="gender" value="pr"> Perempuan
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Tempat Lahir</label>
					   <input type="text" name="place" class="form-control" id="place" placeholder="Tempat lahir anda">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Tempat Lahir</label>
					   <input type="date" name="bday" class="form-control" id="bday" placeholder="Tempat lahir anda">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Alamat</label>
					   <textarea class="form-control" name="add" id="add" placeholder="Alamat Anda"></textarea>
					</div>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</form>				
			</div>
		
			<?php 
				if ($data == true && $data != 'kosong') {
			?>
				<div class="col-md-7">
					<div>
						<br>
						<h4>Biodata Anda</h4>
						<table class="table table-striped">
							<tr>
								<td>Nama</td>
								<td>:</td>
								<td><?=$name;?></td>
							</tr>

							<tr>
								<td>Jenis Kelamin</td>
								<td>:</td>
								<td><?=$gender == 'lk' ? 'Laki-laki' : 'Perempuan';?></td>
							</tr>

							<tr>
								<td>Tempat, Tanggal Lahir</td>
								<td>:</td>
								<td><?=$place;?>, <?=$bday;?></td>
							</tr>

							<tr>
								<td>Usia</td>
								<td>:</td>
								<td><?=$age;?> Tahun</td>
							</tr>

							<tr>
								<td>Alamat</td>
								<td>:</td>
								<td><?=$add;?></td>
							</tr>				
						</table>
					</div>
				</div>
			<?php		 	
				 } 
			?>
		</div>
	</div>
	<!-- Footer -->
	<?php $this->load->view('packages/footer'); ?>
	<script type="text/javascript">
		function validation(){
			var name = $('#name');
			var gender = $('#gender');
			var genderCheck = false;
			var place = $('#place');
			var bday = $('#bday').val();
			var add = $('#add');
			// console.log(bday);
			// return false;
			if (name.val() == '') {
				alert('Nama wajib diisi');
				name.focus();
				return false;
			}

			for(var i=0; i<gender.length;i++){
	            if(gender[i].checked == true){
					genderCheck = true;	            	     
	            }
	        }

	        if (genderCheck == false) {
	        	alert('Jenis kelamin wajib diisi');
	            return false;
	        }

	        if (place.val() == '') {
	        	alert('Tempat lahir wajib diisi');
				place.focus();
				return false;
	        }

	        if (bday == '') {
	        	alert('Tanggal lahir wajib diisi');
				return false;
	        }

	        if (add.val() == '') {
	        	alert('Alamat wajib diisi');
	        	add.focus();
				return false;
	        }
		}	
	</script>
	
</body>
</html>
