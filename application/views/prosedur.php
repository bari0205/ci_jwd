<!DOCTYPE html>
<html lang="en">
<head>
	<title>Pertemuan 8 | Functional Procedure</title>
	<?php $this->load->view('packages/head'); ?>
</head>
<body>
	<?php $this->load->view('parts/header'); ?>
	<div class="container mt-3">
		<div class="row justify-content-start">

			<div class="col-md-12">
				<h5>Fungsional Prosedur</h5>
			</div>
			<div class="col-md-12">
				<?php 
				function perkenalan(){
					echo "Selamat datang, ";
					echo "Pada acara Digital Talent<br>";
					echo "2022<br>";
				}
				perkenalan();
				?>
			</div>
			<div class="col-md-12">
				<br>
				<hr>
				<br>
				<h5>Parameter</h5>	
			</div>
			<div class="col-md-12">
				<?php 
				function parameter($nama, $salam){
					echo $salam.', ';
					echo "Nama saya ".$nama."<br>";
					echo "Senang berkenalan dengan anda<br>";
				}
				$saya = 'Akbar';
				$salam = 'Hi selamat sore';
				parameter($saya, $salam);

				?>
			</div>
			<div class="col-md-12">
				<br>
				<hr>
				<br>
				<h5>Nilai Default</h5>
			</div>
			<div class="col-md-12">
				<?php 
					function perkenalandiri($nama, $salam='Selamat datang'){
					echo $salam.', ';
					echo "Nama saya ".$nama."<br>";
					echo "Senang berkenalan dengan anda<br>";
				}

					perkenalandiri("Akbar");
				?>
			</div>
			<div class="col-md-12">
				<br>
				<hr>
				<br>
				<h5>Mengembalikan Nilai</h5>
				<br>
			</div>
			<div class="col-md-12">
				<?php
				function umur($tahunlahir, $tahunsekarang){
					$umur = $tahunsekarang - $tahunlahir;
					return $umur;
				}
				echo "Umur saya adalah ".umur(1998, 2022);
				?>
			</div>
			<div class="col-md-12">
				<br>
				<hr>
				<br>
				<h5>Function dalam Function</h5>
				<br>
			</div>
			<div class="col-md-12">
				<?php
				function hitungumur($tahunlahir, $tahunsekarang){
					$umur = $tahunsekarang - $tahunlahir;
					return $umur;
				}

				function default_string($nama, $salam='Selamat datang'){
					echo $salam.', ';
					echo "Nama saya ".$nama."<br>";
					echo "Senang berkenalan dengan anda<br>";
					echo "Umur saya ".hitungumur(1998, 2022);
				}

				default_string("Akbar");
				?>
			</div>

			<div class="col-md-12">
				<br>
				<hr>
				<br>
				<h5>Function Rekursif</h5>
				<br>
			</div>
			<div class="col-md-12">
				<?php
				function factorial($angka){
					if ($angka < 2 ) {
						return 1;
					}else{
						return ($angka * factorial($angka-1));
					}
				}

				echo "Faktorial 5 adalah ".factorial(5);
				?>
			</div>

			<div class="col-md-12">
				<br>
				<hr>
				<br>
				<h5>Procedurial didalam PHP</h5>
				<br>
			</div>
			<div class="col-md-12">
				<?php
				function do_print(){
					echo time();
				}

				do_print();
				echo "<br>";

				function jumlah($a, $b){
					return ($a + $b);
				}

				echo jumlah(2,3);
				?>
			</div>
			<br><br>

		</div>
	</div>
	<!-- Footer -->
	<?php $this->load->view('packages/footer'); ?>
</body>
</html>
