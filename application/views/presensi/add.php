<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tugas Data Entry</title>
	<?php $this->load->view('packages/head'); ?>
</head>
<body>
	<?php $this->load->view('parts/header'); ?>
	<div class="container mt-3">
		<div class="row">
			<div class="col-md-5">
				<h2>Tambah Data Presensi</h2>
				<!-- <br> -->
				<form method="POST" action="<?=base_url();?>presensi/create" onsubmit="return validation()" enctype="multipart/form-data">
					<div class="form-group">
					   <label for="exampleInputEmail1">Peserta*</label>
					   <select class="form-control" id="peserta_select" name="id_peserta" required="">
					   	<option value="">-- Pilih Peserta --</option>
					   	<?php foreach ($data_peserta as $peserta){?>
					   		<option value="<?=$peserta->id;?>"><?=$peserta->name;?></option>
					   	<?php } ?>
					   </select>
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Kelas*</label>
					   <input type="text" name="class" class="form-control" id="class" placeholder="Kelas Peserta" required="">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Pertemuan Ke*</label>
					   <input type="number" name="meet" class="form-control" id="meet" placeholder="Pertemuan ke" required="">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Materi*</label>
					   <input type="text" name="materi" class="form-control" id="materi" placeholder="Materi" required="">
					</div>

					<div class="form-group">
					   <label for="exampleInputEmail1">Pengajar*</label>
					   <select class="form-control" id="pengajar_select" name="id_pengajar" required="">
					   	<option value="">-- Pilih Pengajar --</option>
					   	<?php foreach ($data_pengajar as $pengajar){?>
					   		<option value="<?=$pengajar->id;?>"><?=$pengajar->name;?></option>
					   	<?php } ?>
					   </select>
					</div>

					<div>
                      <label for="exampleInputEmail1" class="form-label">Bukti Hadir<span class="required-fill">*</span></label>
                      <div>
                        <!-- <a id="link-zoom" href="<?=base_url()?>assets/uploads/default-image.jpg" class="gallery-imgswiper-zoom gallery-lightbox"> -->
                          <img id="preview-image" style="max-width: 245px; margin-bottom: 1%;" src="<?=base_url()?>assets/uploads/default-image.jpg">
                        <!-- </a> -->
                      </div>
                      
                    </div>
                    <div>
                      <input type="file" class="form-control input-length" id="bukti" name="bukti" style="display: inline;" onchange="previewImage(event)" accept="image/*" required="">
                      <div id="" class="form-text">Format: jpg, jpeg, png</div>
                    </div>

					<button type="submit" class="btn btn-primary">Simpan</button>
				</form>				
			</div>
		</div>
	</div>
	<!-- Footer -->
	<?php $this->load->view('packages/footer'); ?>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#peserta_select').select2();
		});
		$(document).ready(function() {
		    $('#pengajar_select').select2();
		});
		
		var previewImage = function(event) {
	      var reader = new FileReader();
	      reader.onload = function(){
	        var output = document.getElementById('preview-image');
	        var link = document.getElementById('link-zoom');
	        output.src = reader.result;
	        link.href = reader.result;
	      };
	      reader.readAsDataURL(event.target.files[0]);
	      // galleryLightbox.reload();
	    };
	</script>
</body>
</html>
