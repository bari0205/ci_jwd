<!DOCTYPE html>
<html lang="en">
<head>
	<title>Pertemuan 7 | Order Page</title>
	<?php $this->load->view('packages/head'); ?>
</head>
<body>
	<?php $this->load->view('parts/header'); ?>
	<div class="container mt-3">
		<div class="row justify-content-center">
			<div class="card shadow-sm my-5" style="width: 30rem;">
			  <div class="card-body">
			    <h5 class="card-title" style="text-align: center; margin-bottom: 25px;">Pesanan</h5>
			    <div>
			    	<div class="card shadow-sm space-card" id="row-cappucino" style="width: 100%;">
					  <div class="card-body">
					    <div class="row" id="cappucino">
					    	<div class="col-md-6">Cappucino</div>
					    	<div class="col-md-6 price-card">Rp. 35.000</div>
					    	<input type="hidden" name="cappucino1" id="cappucino1" value="35000">
					    </div>
					  </div>
					</div>	

					<div class="card shadow-sm space-card" id="row-gtl" style="width: 100%;">
					  <div class="card-body">
					    <div class="row" id="gtl">
					    	<div class="col-md-6">Green Tea Latte</div>
					    	<div class="col-md-6 price-card">Rp. 40.000</div>
					    	<input type="hidden" name="gtl1" id="gtl1" value="40000">
					    </div>
					  </div>
					</div>	

					<div class="card shadow-sm space-card" id="row-fc" style="width: 100%;">
					  <div class="card-body">
					    <div class="row" id="fc">
					    	<div class="col-md-6">Fish & Chips</div>
					    	<div class="col-md-6 price-card">Rp. 50.000</div>
					    	<input type="hidden" name="fc1" id="fc1" value="50000">
					    </div>
					  </div>
					</div>	

					<div class="card shadow-sm space-card" id="row-tuna-s" style="width: 100%;">
					  <div class="card-body">
					    <div class="row" id="tuna-s">
					    	<div class="col-md-6">Tuna Sandwich</div>
					    	<div class="col-md-6 price-card">Rp. 45.000</div>
					    	<input type="hidden" name="tuna-s1" id="tuna-s1" value="45000">
					    </div>
					  </div>
					</div>

					<div class="card shadow-sm space-card" id="row-mw" style="width: 100%;">
					  <div class="card-body">
					    <div class="row" id="mw">
					    	<div class="col-md-6">Mineral Water</div>
					    	<div class="col-md-6 price-card">Rp. 8.000</div>
					    	<input type="hidden" name="mw1" id="mw1" value="8000">
					    </div>
					  </div>
					</div>	

					<div class="card shadow-sm space-card" id="row-ff" style="width: 100%;">
					  <div class="card-body">
					    <div class="row" id="ff">
					    	<div class="col-md-6">French Fries</div>
					    	<div class="col-md-6 price-card">Rp. 18.000</div>
					    	<input type="hidden" name="ff1" id="ff1" value="18000">
					    </div>
					  </div>
					</div>	

					<div class="row row-total">
						<div class="col col-md-6"><h4>Total :</h4></div>
						<div class="col col-md-6 price-card"><h4>Rp. <span id="total">0</span></h4></div>
					</div>
			    </div>
			    
			  </div>
			</div>
		</div>
	</div>
	<!-- Footer -->
	<?php $this->load->view('packages/footer'); ?>
	<script type="text/javascript">
		var cappucino_val = 0;
		var gtl_val = 0;
		var fc_val = 0;
		var tunas_val = 0;
		var mw_val = 0;
		var ff_val = 0;
		var total = 0;

		$('#row-cappucino').click(function(){
			if (cappucino_val == 0) {
				$('#row-cappucino').addClass("active-card");
				cappucino_val = $('#cappucino1').val();
				total += Number(cappucino_val);
				change_total(total)
			}else{
				$('#row-cappucino').removeClass("active-card");
				total -= Number(cappucino_val);
				change_total(total)
				cappucino_val = 0;
			}
		});

		$('#row-gtl').click(function(){
			if (gtl_val == 0) {
				$('#row-gtl').addClass("active-card");
				gtl_val = $('#gtl1').val();
				total += Number(gtl_val);
				change_total(total)
			}else{
				$('#row-gtl').removeClass("active-card");
				total -= Number(gtl_val);
				change_total(total)
				gtl_val = 0;
			}
		});


		$('#row-fc').click(function(){
			if (fc_val == 0) {
				$('#row-fc').addClass("active-card");
				fc_val = $('#fc1').val();
				total += Number(fc_val);
				change_total(total)
			}else{
				$('#row-fc').removeClass("active-card");
				total -= Number(fc_val);
				change_total(total)
				fc_val = 0;
			}
		});

		$('#row-tuna-s').click(function(){
			if (tunas_val == 0) {
				$('#row-tuna-s').addClass("active-card");
				tunas_val = $('#tuna-s1').val();
				total += Number(tunas_val);
				change_total(total)
			}else{
				$('#row-tuna-s').removeClass("active-card");
				total -= Number(tunas_val);
				change_total(total)
				tunas_val = 0;
			}
		});

		$('#row-mw').click(function(){
			if (mw_val == 0) {
				$('#row-mw').addClass("active-card");
				mw_val = $('#mw1').val();
				total += Number(mw_val);
				change_total(total)
			}else{
				$('#row-mw').removeClass("active-card");
				total -= Number(mw_val);
				change_total(total)
				mw_val = 0;
			}
		});

		$('#row-ff').click(function(){
			if (ff_val == 0) {
				$('#row-ff').addClass("active-card");
				ff_val = $('#ff1').val();
				total += Number(ff_val);
				change_total(total)
			}else{
				$('#row-ff').removeClass("active-card");
				total -= Number(ff_val);
				change_total(total)
				ff_val = 0;
			}
		});

		function change_total(total){
			$('#total').html(total); 
		}
	</script>
	
</body>
</html>
