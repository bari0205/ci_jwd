<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">DIGI TALENT</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Home</a>
      </li>

      <li class="nav-item <?=$cur_page == 'biodata' ? 'active' : '';?>">
        <a class="nav-link" href="<?=base_url();?>biodata">Biodata</a>
      </li>

      <li class="nav-item <?=$cur_page == 'order' ? 'active' : '';?>">
        <a class="nav-link" href="<?=base_url();?>order">Form Order</a>
      </li>   

       <li class="nav-item dropdown <?=$page == 'pert8' ? 'active' : '';?>">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Functional PHP
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item <?=$cur_page == 'proc' ? 'active' : '';?>" href="<?=base_url();?>prosedur">Latihan</a>
          <a class="dropdown-item <?=$cur_page == 'parse_error' ? 'active' : '';?>" href="<?=base_url();?>parse_error">Parse Error</a>
          <a class="dropdown-item <?=$cur_page == 'fatal_error' ? 'active' : '';?>" href="<?=base_url();?>fatal_error">Fatal Error</a>
          <a class="dropdown-item <?=$cur_page == 'warning_error' ? 'active' : '';?>" href="<?=base_url();?>warning_error">Warning Error</a>
          <a class="dropdown-item <?=$cur_page == 'notice_error' ? 'active' : '';?>" href="<?=base_url();?>notice_error">Notice Error</a>
          <!-- 
          <a class="dropdown-item <?=$cur_page == 'param' ? 'active' : '';?>" href="<?=base_url();?>parameter"> Parameter</a>
          <a class="dropdown-item <?=$cur_page == 'default' ? 'active' : '';?>" href="<?=base_url();?>default_string"> Nilai Default</a>
          <a class="dropdown-item <?=$cur_page == 'kembali' ? 'active' : '';?>" href="<?=base_url();?>kembali"> Pengembalian Nilai</a>
          <a class="dropdown-item <?=$cur_page == 'funcin' ? 'active' : '';?>" href="<?=base_url();?>funcin"> Function in Function</a>
          <a class="dropdown-item <?=$cur_page == 'rekursif' ? 'active' : '';?>" href="<?=base_url();?>rekursif"> Rekursif</a>
          <a class="dropdown-item <?=$cur_page == 'procin' ? 'active' : '';?>" href="<?=base_url();?>procin"> Procedure in PHP</a> -->
        </div>
      </li>   

      <!-- <li class="nav-item <?=$cur_page == 'peserta' ? 'active' : '';?>">
        <a class="nav-link" href="<?=base_url();?>peserta">Peserta</a>
      </li>   -->

      <li class="nav-item dropdown <?=$page == 'crud_func' ? 'active' : '';?>">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Tugas CRUD
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item <?=$cur_page == 'peserta' ? 'active' : '';?>" href="<?=base_url();?>peserta">Peserta</a>
          <a class="dropdown-item <?=$cur_page == 'pengajar' ? 'active' : '';?>" href="<?=base_url();?>pengajar">Pengajar</a>
          <a class="dropdown-item <?=$cur_page == 'presensi' ? 'active' : '';?>" href="<?=base_url();?>presensi">Presensi</a>
        </div>
      </li>  

    </ul>
    <form class="form-inline my-2 my-lg-0">
      <!-- <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"> -->
      <a class="btn btn-outline-success my-2 my-sm-0" href="<?=base_url();?>login/logout">Logout</a>
    </form>
  </div>
</nav>