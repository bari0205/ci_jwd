<?php 
class T_Pengajar extends CI_Model {

  public function getAll()
  {
    $query = $this->db->select('*');
    $query = $query->where('remove', 'N');
    $query = $query->get('pengajar');
    return $query->result();
  }

  public function getDetail($id)
  {
    $query = $this->db->select('*');
    $query = $query->where('id', $id);
    $query = $query->limit(1);
    $query = $query->get('pengajar');
    return $query->result();
  }

  public function checkNum($no_regis)
  {
    $query = $this->db->select('id');
    $query = $query->where('no_regis', $no_regis);
    $query = $query->where('remove', 'N');
    $query = $query->limit(1);
    $query = $query->get('pengajar');
    return $query->result();
  }

  public function insert($data,$table){
    $insert = $this->db->insert($table, $data);
    return $insert;
  }

  public function update($data, $id)
  {
    $query =  $this->db->where('id', $id);
    $query =  $query->update('pengajar', $data);
    return $query;
  }

  public function delete($id)
  { 
    $query =  $this->db->set('remove', 'Y');
    $query =  $query->where('id', $id);
    $query =  $query->update('pengajar');
    return $query;
  }

}
