<?php 
class T_Presensi extends CI_Model {

  public function getAll()
  {
    $query = $this->db->select('a.id, a.id_peserta, a.id_pengajar, b.name nama_peserta, b.no_regis, c.name nama_pengajar, a.tgl_hadir, a.pertemuan_ke, a.kelas, a.materi, a.bukti, a.created_date, a.updated_date');
    $query = $query->join('peserta b', 'a.id_peserta = b.id', 'left');
    $query = $query->join('pengajar c', 'a.id_pengajar = c.id', 'left');
    $query = $query->where('a.remove', 'N');
    $query = $query->where('b.remove', 'N');
    $query = $query->get('presensi a');
    return $query->result();
  }

  public function getDetail($id)
  {
    $query = $this->db->select('*');
    $query = $query->where('id', $id);
    $query = $query->limit(1);
    $query = $query->get('presensi');
    return $query->result();
  }

  public function check($id_peserta)
  {
    $start_now = date('Y-m-d 00:00:01');
    $end_now = date('Y-m-d 23:59:59');
    $query = $this->db->select('id');
    $query = $query->where('id_peserta', $id_peserta);
    $query = $query->where("(tgl_hadir>='".$start_now."' AND tgl_hadir<='".$end_now."')");
    $query = $query->where('remove', 'N');
    $query = $query->limit(1);
    $query = $query->get('presensi');
    return $query->result();
  }

  public function insert($data,$table){
    $insert = $this->db->insert($table, $data);
    return $insert;
  }

  public function update($data, $id)
  {
    $query =  $this->db->where('id', $id);
    $query =  $query->update('presensi', $data);
    return $query;
  }

  public function delete($id)
  { 
    $query =  $this->db->set('remove', 'Y');
    $query =  $query->where('id', $id);
    $query =  $query->update('presensi');
    return $query;
  }

}
